package com.example.averagescore;

public class CourseModel {

    private String name;
    private double score;
    private double unit;

    public CourseModel() {
    }

    public CourseModel(String name, double score, double unit) {
        this.name = name;
        this.score = score;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getUnit() {
        return unit;
    }

    public void setUnit(double unit) {
        this.unit = unit;
    }
}

package com.example.averagescore.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.averagescore.Contracts;
import com.example.averagescore.CourseModel;
import com.example.averagescore.R;
import com.example.averagescore.presenters.MainActivityPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        Contracts.View, FragmentToActivity {

    private ListViewAdapter listViewAdapter;
    private Button addButton;
    private Button equalButton;
    private MainActivityPresenter presenter;
    private FragmentManager fragmentManager;
    private AddDialogFragment addDialogFragment;
    private EqualDialogFragment equalDialogFragment;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenter();
        bind();
        presenter.attachView(this);
    }

    private void bind() {
        listView = findViewById(R.id.listView);
        addButton = findViewById(R.id.addButton);
        equalButton = findViewById(R.id.equalButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.showAddDialog();

            }
        });
        equalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onEqualOrder();
            }
        });
    }

    @Override
    public void addToList(List<CourseModel> courseModels) {
        listViewAdapter = new ListViewAdapter(this, courseModels);
        listView.setAdapter(listViewAdapter);
    }

    @Override
    public void showAddDialog() {
        fragmentManager = getSupportFragmentManager();
        addDialogFragment = AddDialogFragment.newInstance("title");
        addDialogFragment.show(fragmentManager, "fragment");
        addDialogFragment.setCancelable(false);
    }

    @Override
    public void showEqualDialog(double results) {
        Bundle bundle = new Bundle();
        bundle.putString("result", String.valueOf(results));
        fragmentManager = getSupportFragmentManager();
        equalDialogFragment = EqualDialogFragment.newInstance("title");
        equalDialogFragment.show(fragmentManager, "fragment");
        equalDialogFragment.setArguments(bundle);
    }

    @Override
    public void hideDialog() {
        addDialogFragment.dismiss();
    }

    @Override
    public void onError() {

    }

    @Override
    public void sendData(String courseName, double score, double units) {
        presenter.onAddOrder(courseName, score, units);
    }
}

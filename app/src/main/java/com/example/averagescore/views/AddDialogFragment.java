package com.example.averagescore.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.averagescore.R;

import java.util.Objects;


public class AddDialogFragment extends DialogFragment {

    private EditText editText;
    private EditText editText2;
    private String fragCourseName;
    private double fragScore;
    private TextSwitcher textSwitcher;
    private double fragUnits = 0.5;
    private FragmentToActivity fragmentToActivity;

    public AddDialogFragment() {
    }

    static AddDialogFragment newInstance(String title) {
        AddDialogFragment fragment = new AddDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        args.putString("title", title);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentToActivity = (FragmentToActivity) context;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_add_dialog, null);

        editText = view.findViewById(R.id.courseName_editText);
        SeekBar seekBar = view.findViewById(R.id.seekBar);
        editText2 = view.findViewById(R.id.score_editText);
        Button addButton = view.findViewById(R.id.fragmentButton_add);
        Button cancelButton = view.findViewById(R.id.fragmentButton_cancel);
        textSwitcher = view.findViewById(R.id.textSwitcher);
        textSwitcher.setInAnimation(loadInAnimation());
        textSwitcher.setOutAnimation(loadOutAnimation());
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(getActivity());
                textView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                textView.setTextSize(36);
                return textView;
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i == 0) {
                    fragUnits = 0.5;
                    textSwitcher.setText(String.valueOf(fragUnits));
                } else {
                    fragUnits = i;
                    textSwitcher.setText(String.valueOf(i));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        textSwitcher.setCurrentText(String.valueOf(fragUnits));
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp;
                fragCourseName = editText.getText().toString();
                temp = editText2.getText().toString();
                if (!fragCourseName.equals("") && !temp.equals("")) {
                    fragScore = Double.valueOf(temp);
                    sendDataToActivity(fragCourseName, fragScore, fragUnits);
                    Log.wtf("why", "onClick: " + fragCourseName + "  " + fragScore + "  " + fragUnits);
                    dismiss();

                } else {
                    onEmpty();
                    Log.d("empty", "onClick: every thing must be empty  " + fragCourseName
                            + "  " + fragScore);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        builder.setView(view).setTitle("Enter Scores");
        return builder.create();
    }


    private void onEmpty() {
        Toast.makeText(getActivity(), "Cant be Empty", Toast.LENGTH_SHORT).show();
    }

    private Animation loadOutAnimation() {

        return AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);
    }

    private Animation loadInAnimation() {
        return AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);

    }

    private void sendDataToActivity(String courseName, double score, double units) {
        fragmentToActivity.sendData(courseName, score, units);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentToActivity = null;
    }
}




package com.example.averagescore.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.averagescore.R;


public class EqualDialogFragment  extends DialogFragment {


    public static EqualDialogFragment newInstance(String title) {
        EqualDialogFragment fragment = new EqualDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        args.putString("title", title);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String result = (getArguments()).getString("result");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_equal_dialog, null);
        TextView textView = view.findViewById(R.id.equal_TextView);

        textView.setText(result);

        builder.setView(view).setTitle("Results Are : ");

        return builder.create();


    }
}

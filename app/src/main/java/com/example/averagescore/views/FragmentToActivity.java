package com.example.averagescore.views;

interface FragmentToActivity {
    void sendData (String courseName , double score , double units);
}

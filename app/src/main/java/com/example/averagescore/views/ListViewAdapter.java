package com.example.averagescore.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.averagescore.CourseModel;
import com.example.averagescore.R;

import java.util.List;

class ListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<CourseModel> courseModelList;

    private String courseName;
    private double score;
    private double unit;

    public ListViewAdapter() {
    }

    public ListViewAdapter(Context context, List<CourseModel> courseModelList) {
        this.mContext = context;
        this.courseModelList = courseModelList;
    }

    @Override
    public int getCount() {
        return courseModelList.size();
    }

    @Override
    public Object getItem(int position) {

        return courseModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.list_row, viewGroup, false);

        courseName = courseModelList.get(position).getName();
        score = courseModelList.get(position).getScore();
        unit = courseModelList.get(position).getUnit();
        TextView courseNameList;
        TextView scoreList;
        TextView unitList;
        courseNameList = view.findViewById(R.id.courseName_list);
        scoreList = view.findViewById(R.id.score_list);
        unitList = view.findViewById(R.id.unit_list);

        courseNameList.setText(courseName);
        scoreList.setText(String.valueOf(score));
        unitList.setText(String.valueOf(unit));

        return view;
    }
}

package com.example.averagescore;

import java.util.List;

public interface Contracts {


    interface View {

        void addToList(List<CourseModel> courseModels);
        void showAddDialog();
        void showEqualDialog(double results);
        void hideDialog();

        void onError();
    }

    interface Presenter {
        void attachView(View v);

        void onAddOrder(String courseName , double score , double unit);
        void addToList(List<CourseModel> courseModels);
        void onError();

        void onEqualOrder();
        void showEqualDialog(double results);
        void showAddDialog();

        void hideDialog();

    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void onAddOrder(String courseName , double score , double unit);
        void onEqualOrder();
    }
}

package com.example.averagescore;

import java.util.ArrayList;
import java.util.List;

public class Model implements Contracts.Model {
    private Contracts.Presenter presenter;
    private double unitSum;
    private double scoreAndUnits = 0;
    private List<CourseModel> courseModelList = new ArrayList<>();

    @Override
    public void attachPresenter(Contracts.Presenter presenter) {
        this.presenter = presenter;

    }

    @Override
    public void onAddOrder(String courseName, double score, double unit) {
        CourseModel courseModel = new CourseModel();
        courseModel.setName(courseName);
        courseModel.setScore(score);
        courseModel.setUnit(unit);
        courseModelList.add(courseModel);
        presenter.addToList(courseModelList);
    }

    @Override
    public void onEqualOrder() {
        double results = 0 ;
        for (int i = 0; i < courseModelList.size(); i++) {

            scoreAndUnits += courseModelList.get(i).getUnit() * courseModelList.get(i).getScore();
            unitSum += courseModelList.get(i).getUnit();
             results = scoreAndUnits / unitSum;
        }
        presenter.showEqualDialog(results);

    }
}

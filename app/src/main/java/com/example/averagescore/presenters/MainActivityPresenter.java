package com.example.averagescore.presenters;

import com.example.averagescore.Contracts;
import com.example.averagescore.CourseModel;
import com.example.averagescore.Model;

import java.util.List;

public class MainActivityPresenter implements Contracts.Presenter {
    private Contracts.View view;
    private final Contracts.Model model = new Model();

    @Override
    public void attachView(Contracts.View v) {
        this.view = v;
        model.attachPresenter(this);
    }

    @Override
    public void onAddOrder(String courseName, double score, double unit) {
        model.onAddOrder(courseName, score, unit);

    }

    @Override
    public void addToList(List<CourseModel> courseModels) {
        view.addToList(courseModels);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onEqualOrder() {
        model.onEqualOrder();
    }

    @Override
    public void showEqualDialog(double results) {
        view.showEqualDialog(results);
    }

    @Override
    public void showAddDialog() {
        view.showAddDialog();
    }

    @Override
    public void hideDialog() {
        view.hideDialog();
    }
}
